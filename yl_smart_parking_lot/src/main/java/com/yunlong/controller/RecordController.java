package com.yunlong.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.yunlong.dao.BillRepository;
import com.yunlong.dao.RecordRepository;
import com.yunlong.entity.Bill;
import com.yunlong.entity.Record;
import com.yunlong.tool.Json;

@RestController
@RequestMapping("/parking")
public class RecordController {
	@Autowired RecordRepository recordRepository;
	@Autowired BillRepository billRepository;
	
	/*
	 * 扫描车牌号准备进入停车场
	 */
	@PostMapping(value = "/enter")
    public Json enter(@RequestParam("plateNumber") String plateNumber) {
		System.out.print(plateNumber);
		
		if (plateNumber == null || plateNumber == "") {
			return Json.getInstance().fail("车牌号不能为空");
		}
		
		Record record = recordRepository.findByPlateNumber(plateNumber);
		System.out.print(record);
		
		Date date = new Date();
		if (record == null) {
			record = new Record();
			record.setEnterTime(date);
			record.setPlateNumber(plateNumber);
		}else {
			record.setEnterTime(date);
			record.setLeaveTime(date);
			record.setIsPay(0);
			record.setMoney(0.0);
			record.setPayTime(date);
			record.setPlateNumber(plateNumber);
		}
		recordRepository.save(record);

		return Json.getInstance().success("成功");
    }
	/*
	 * 扫描车牌号准备离开停车场
	 */
	@PostMapping("/leave")
    public Json leave(@RequestParam("plateNumber") String plateNumber) {
		System.out.print(plateNumber);
		
		if (plateNumber == null || plateNumber == "") {
			return Json.getInstance().fail("车牌号不能为空");
		}
		
		Record record = recordRepository.findByPlateNumber(plateNumber);
		System.out.print(record);
		if (record == null) {
			return Json.getInstance().fail("没有查找到该条数据");
		}else if (record.getIsPay() == 0) {//未支付
			record.setLeaveTime(new Date());
			recordRepository.save(record);
			return Json.getInstance().fail("请先支付再离开");
		}else if (record.getIsPay() == 1) {//已支付
			Date leaveTime = new Date();
			Date payTime = record.getLeaveTime();
			Long diffHour = (leaveTime.getTime()-payTime.getTime()) / (1000 * 60 * 60);
			if (diffHour > 1) {//判断支付时间和离开时间 防止投机支付-先支付车不走
				record.setEnterTime(payTime);
				record.setIsPay(0);
				recordRepository.save(record);
				return Json.getInstance().fail("该车支付后停留超过1小时，不可以放行");
			}else {
				return Json.getInstance().success("该车已经支付可以放行");
			}
		}else {
			return Json.getInstance().success("失败");
		}
    }
	/*
	 * 支付，微信或者支付宝
	 */
	@PostMapping("/pay")
    public Json pay(@RequestParam("plateNumber") String plateNumber) {
        System.out.print(plateNumber);
		
		if (plateNumber == null || plateNumber == "") {
			return Json.getInstance().fail("车牌号不能为空");
		}
		
		Record record = recordRepository.findByPlateNumber(plateNumber);
		System.out.print(record);
		
		Date date = new Date();
		if (record == null) {
			return Json.getInstance().fail("没有查找到该条数据");
		}else {
			Date enterTime = record.getEnterTime();
			Long diffHour = (date.getTime()-enterTime.getTime()) / (1000 * 60 * 60);
			double money = diffHour*2;
			
			record.setIsPay(1);
			record.setPayTime(date);
			record.setMoney(money);
			recordRepository.save(record);
			
			Bill bill = new Bill();
			bill.setEnterTime(record.getEnterTime());
			bill.setLeaveTime(record.getLeaveTime());
			bill.setMoney(money);
			bill.setPlateNumber(plateNumber);
			billRepository.save(bill);
			
			return Json.getInstance().success("成功");
		}
    }
	/*
	 * 查询在停车场的停车费
	 */
	@PostMapping("/queryCost")
    public Json queryCost(@RequestParam("plateNumber") String plateNumber) {
        System.out.print(plateNumber);
		
		if (plateNumber == null || plateNumber == "") {
			return Json.getInstance().fail("车牌号不能为空");
		}
		
		Record record = recordRepository.findByPlateNumber(plateNumber);
		System.out.print(record);
		
		if (record == null) {
			return Json.getInstance().fail("没有查找到该条数据");
		}else {
			Date date = new Date();
			Date enterTime = record.getEnterTime();
			Long diffHour = (date.getTime()-enterTime.getTime()) / (1000);
			double money = diffHour*2;
			
			return Json.getInstance().success("成功", money);
		}
    }
	/*
	 * 查询账单
	 */
	@PostMapping("/queryBill")
    public Json queryBill(@RequestParam("plateNumber") String plateNumber) {
        System.out.print(plateNumber);
		
		if (plateNumber == null || plateNumber == "") {
			return Json.getInstance().fail("车牌号不能为空");
		}
		
		List<Bill> list = billRepository.findByPlateNumber(plateNumber);
		System.out.print(list);
		
		if (list.isEmpty() == true) {
			return Json.getInstance().fail("没有查找到该条数据");
		}else {
			HashMap<String, List<Bill>> map = new HashMap<String, List<Bill>>();
			map.put("list", list);
			return Json.getInstance().success("成功", map);
		}
    }
}
