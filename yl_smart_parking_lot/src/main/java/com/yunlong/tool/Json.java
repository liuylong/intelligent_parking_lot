package com.yunlong.tool;

import java.io.Serializable;

import lombok.Data;

@Data
public class Json implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//默认未失败状态
    private static Json instance;
    private String msg = "接口访问失败";
    private int code = 300;
    private Object data = null;
 
    public synchronized static Json getInstance() {
        if(instance==null){
            instance = new Json();
        }
        return instance;
    }
 
    public Json() {
        super();
    }
 
    public Json success(String msg){
        this.msg = msg;
        this.code = 1;
        this.data = "";
        return this;
    }
    
    public Json success(String msg, Object data){
        this.msg = msg;
        this.code = 1;
        this.data = data;
        return this;
    }
 
    public Json fail(String msg, Object data){
        this.msg = msg;
        this.code = 0;
        this.data = data;
        return this;
    }
    
    public Json fail(String msg){
        this.msg = msg;
        this.code = 0;
        this.data = "";
        return this;
    }
}
