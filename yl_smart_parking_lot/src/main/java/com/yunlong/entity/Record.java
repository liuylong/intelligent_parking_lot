package com.yunlong.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "record")
@Data
public class Record {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "plate_number", unique = true, nullable = false, length = 20)
	private String plateNumber;
	
	@Column(name = "enter_time", length = 10)
	private Date enterTime;
	
	@Column(name = "leave_time", length = 10)
	private Date leaveTime;
	
	@Column(name = "money", length = 10)
	private Double money;
	
	@Column(name = "isPay", length = 10)
	private int isPay=0;
	
	@Column(name = "payTime", length = 10)
	private Date payTime;
}
