package com.yunlong.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.yunlong.entity.Record;

public interface RecordRepository extends JpaRepository<Record, Integer> {
	Record findByPlateNumber(String plateNumber);
}
