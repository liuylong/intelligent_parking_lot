package com.yunlong.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.yunlong.entity.Bill;

public interface BillRepository extends JpaRepository<Bill, Integer> {
	List<Bill> findByPlateNumber(String plateNumber);
}
