package com.yunlong.exception;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

@ControllerAdvice
public class GlobalExceptionHandler {
	
	@ExceptionHandler(value = Exception.class)
	public ModelAndView defaultErrorHandler(HttpServletRequest req, Exception e) throws Exception {
        ModelAndView mav = new ModelAndView();
        mav.addObject("exception", e);
        mav.addObject("url", req.getRequestURL());
        mav.setViewName("errorPage");
        return mav;
    }
}
/*
之前，我们介绍过@ModelAttribute和@ExceptionHandler,前者可以往请求的Model里加数据，后者可以接受请求处理方法抛出的异常。但是他们放在
控制器(Controller)里的时候，作用范围是有限的，只管当前控制器里的方法。如果你有几百个控制器，在每个控制器里都加上类似的代码，不免有点冗余
和费劲儿。Spring框架提供了@ControllerAdvice注解，帮助你将其应用到所有的控制器上。

Controller Advice字面上意思是“控制器通知”，Advice除了“劝告”、“意见”之外，还有“通知”的意思。你可以将@ModelAttribute和@ExceptionHandler
标记的方法提取出来，放到一个类里，并将加上@ControllerAdvice，这样，所有的控制器都可以用了：
原文链接
https://fookwood.com/spring-boot-tutorial-11-ca
 */
